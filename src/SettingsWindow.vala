/*
	Copyright (C) 2015-2024. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	contributors may be used to endorse or promote products derived
	from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


	VINARI SOFTWARE 2015-2024
*/

using Gtk;

namespace UI{
	public class SettingsWindow : Gtk.Window{

		private Gtk.Button acceptButton;
		private Gtk.Button cancelButton;

		private Gtk.Entry delayTimeEntry;
		private Gtk.Switch rememberDelaySwitch;

		private Gtk.Label delayTimeLabel;
		private Gtk.Label rememberDelayLabel;

		private GLib.Settings mySettings;

		private uint currentDelayTime;
		private uint previousDelayTime;
		private bool isNumeric;

		public SettingsWindow(Gtk.Window parent){
			this.set_transient_for(parent);
			this.set_modal(true);
			this.set_resizable(false);
			this.set_icon_name("org.vinarisoftware.powerdown");
			this.set_title(_("PowerDown - Vinari Software | Preferences"));
		}

		construct{
			mySettings=new GLib.Settings("org.vinarisoftware.powerdown");
			Gtk.Box mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
			mainBox.set_margin_top(10);
			mainBox.set_margin_bottom(10);
			mainBox.set_margin_start(10);
			mainBox.set_margin_end(10);

			this.currentDelayTime=mySettings.get_uint("delay-time");

			Gtk.Box delayTimeBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Box rememberDelayBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 30);
			Gtk.Box buttonsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Separator separatorI=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);

			cancelButton=new Gtk.Button.with_label(_("Cancel"));
			cancelButton.set_hexpand(true);
			acceptButton=new Gtk.Button.with_label(_("Apply changes"));
			acceptButton.set_hexpand(true);

			delayTimeLabel=new Gtk.Label(_("Amount of time  to delay the\nshutdown or reboot:"));
			rememberDelayLabel=new Gtk.Label(_("Remember last time set:"));

			delayTimeEntry=new Gtk.Entry();
			delayTimeEntry.set_max_length(5);
			delayTimeEntry.set_input_purpose(Gtk.InputPurpose.NUMBER);

			rememberDelaySwitch=new Gtk.Switch();

			delayTimeBox.append(delayTimeLabel);
			delayTimeBox.append(delayTimeEntry);

			rememberDelayBox.append(rememberDelayLabel);
			rememberDelayBox.append(rememberDelaySwitch);

			buttonsBox.append(cancelButton);
			buttonsBox.append(acceptButton);

			rememberDelaySwitch.set_active(mySettings.get_boolean("remember-time"));
			delayTimeEntry.set_text(this.currentDelayTime.to_string());

			cancelButton.clicked.connect(cancelButtonAction);
			acceptButton.clicked.connect(acceptButtonAction);

			mainBox.append(delayTimeBox);
			mainBox.append(rememberDelayBox);
			mainBox.append(separatorI);
			mainBox.append(buttonsBox);

			this.set_child(mainBox);
		}

		public void run(){
			this.present();
		}

		private void cancelButtonAction(){
			this.destroy();
		}

		private void acceptButtonAction(){
			try{
				Regex regex = new Regex ("^[0-9 ]+$");
				isNumeric=regex.match(delayTimeEntry.get_text());
			}catch(Error e){
				error("Error: %s", e.message);
			}

			if(isNumeric){
				previousDelayTime=currentDelayTime;
				currentDelayTime=int.parse(delayTimeEntry.get_text());

				if(currentDelayTime<=40000){
					MainWindow.delayTime=this.currentDelayTime;
					mySettings.set_boolean("remember-time", rememberDelaySwitch.get_active());

					if(rememberDelaySwitch.get_active()==true){
						mySettings.set_uint("delay-time", currentDelayTime);
					}else{
						mySettings.set_uint("delay-time", previousDelayTime);
					}

					this.destroy();
				}
			}
		}
	}
}