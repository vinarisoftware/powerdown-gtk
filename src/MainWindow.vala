/*
	Copyright (C) 2015-2024. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	contributors may be used to endorse or promote products derived
	from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


	VINARI SOFTWARE 2015-2024
*/

using Gtk;
using App.Widgets;

namespace UI {
	[GtkTemplate (ui = "/org/vinarisoftware/powerdown/MainWindow.ui")]
	public class MainWindow : Gtk.ApplicationWindow {

		[GtkChild] private unowned Gtk.HeaderBar headerBar;
		[GtkChild] private unowned Gtk.Box mainBox;
		public static uint delayTime;

		private Gtk.Button forceCancelButton;
		private Gtk.Button shutdownButton;
		private Gtk.Button rebootButton;
		private Gtk.Button cancelButton;
		private bool scheduledAction;

		private Gtk.Image shutdownImage;
		private Gtk.Image rebootImage;
		private Gtk.Image cancelImage;

		private Gtk.Label shutdownLabel;
		private Gtk.Label rebootLabel;
		private Gtk.Label cancelLabel;

		private GLib.Settings mySettings;
		private App.Widgets.MessageDialog msgBox;

		public MainWindow (Gtk.Application app) {
			Object (application: app);
		}

		construct{
			this.set_title("PowerDown - Vinari Software");
			this.set_default_size(400, 250);
			this.set_resizable(false);
			this.set_icon_name("org.vinarisoftware.powerdown");

			this.scheduledAction=false;

			mySettings=new GLib.Settings("org.vinarisoftware.powerdown");
			delayTime=mySettings.get_uint("delay-time");

			if(!this.checkInit()){
				forceCancelButton=new Gtk.Button.with_label(_("Force cancel"));
				headerBar.pack_start(forceCancelButton);
				forceCancelButton.clicked.connect(cancelButtonAction);
			}

			Gtk.Box actionButtonsBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 15);
			Gtk.Box shutdownButtonBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
			Gtk.Box rebootButtonBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
			Gtk.Box cancelButtonBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);

			shutdownButtonBox.set_halign(Gtk.Align.CENTER);
			shutdownButtonBox.set_valign(Gtk.Align.CENTER);

			rebootButtonBox.set_halign(Gtk.Align.CENTER);
			rebootButtonBox.set_valign(Gtk.Align.CENTER);

			cancelButtonBox.set_halign(Gtk.Align.CENTER);
			cancelButtonBox.set_valign(Gtk.Align.CENTER);

			shutdownImage=new Gtk.Image.from_resource("/org/vinarisoftware/powerdown/../Assets/ShutDown.png");
			shutdownImage.set_pixel_size(70);

			rebootImage=new Gtk.Image.from_resource("/org/vinarisoftware/powerdown/../Assets/Reboot.png");
			rebootImage.set_pixel_size(70);

			cancelImage=new Gtk.Image.from_resource("/org/vinarisoftware/powerdown/../Assets/Cancel.png");
			cancelImage.set_pixel_size(70);

			shutdownLabel=new Gtk.Label(_("Shutdown"));
			rebootLabel=new Gtk.Label(_("Reboot"));
			cancelLabel=new Gtk.Label(_("Cancel"));

			shutdownButtonBox.append(shutdownImage);
			shutdownButtonBox.append(shutdownLabel);

			rebootButtonBox.append(rebootImage);
			rebootButtonBox.append(rebootLabel);

			cancelButtonBox.append(cancelImage);
			cancelButtonBox.append(cancelLabel);

			shutdownButton=new Gtk.Button();
			shutdownButton.set_child(shutdownButtonBox);
			shutdownButton.set_vexpand(true);
			shutdownButton.set_hexpand(true);

			rebootButton=new Gtk.Button();
			rebootButton.set_child(rebootButtonBox);
			rebootButton.set_vexpand(true);
			rebootButton.set_hexpand(true);

			cancelButton=new Gtk.Button();
			cancelButton.set_child(cancelButtonBox);
			cancelButton.set_vexpand(true);
			cancelButton.set_hexpand(true);

			// Conditional to check if a shutdown process is scheduled (Only works on SystemD)
			if(GLib.FileUtils.test("/run/systemd/shutdown/scheduled", GLib.FileTest.IS_REGULAR)){
				cancelButton.set_sensitive(true);
			}else{
				cancelButton.set_sensitive(false);
			}

			shutdownButton.clicked.connect(shutdownButtonAction);
			rebootButton.clicked.connect(rebootButtonAction);
			cancelButton.clicked.connect(cancelButtonAction);

			actionButtonsBox.append(shutdownButton);
			actionButtonsBox.append(rebootButton);

			mainBox.append(actionButtonsBox);
			mainBox.append(cancelButton);

			if(Posix.getuid()!=0){
				msgBox=new App.Widgets.MessageDialog(this, WARNING, OK, _("PowerDown has been executed without root privileges, it is possible that the software will not work as intended."));
			}
		}

		private void notifyActionToUser(string titleToNotify, string messageToNotify){
			if(Posix.getuid()!=0){
				generateNotification("org.vinarisoftware.powerdown", titleToNotify, messageToNotify);
			}else{
				msgBox=new App.Widgets.MessageDialog(this, INFO, OK, messageToNotify);
			}
		}

		private bool checkScheduledAction(){
			if(this.scheduledAction==true){
				msgBox=new App.Widgets.MessageDialog(this, WARNING, OK, _("There is already an scheduled action.\nPlease cancel it, and try again."));
				return true;
			}

			return false;
		}

		private void shutdownButtonAction(){
			if(this.checkScheduledAction()){
				return;
			}

			string[] shutdownArgs={"shutdown", "-h", delayTime.to_string()};
			string[] spawn_env=Environ.get ();
			Pid child_pid;

			try{
				Process.spawn_async ("/", shutdownArgs, spawn_env, SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD, null, out child_pid);
			}catch(SpawnError e){
				msgBox=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error has occurred while trying to power off the computer..."));
				return;
			}

			cancelButton.set_sensitive(true);
			this.scheduledAction=true;
			this.notifyActionToUser(_("Shuting down"), _("The computer will shutdown in ")+delayTime.to_string()+_(" minute(s)."));
		}

		private void rebootButtonAction(){
			if(this.checkScheduledAction()){
				return;
			}

			string[] rebootArgs={"shutdown", "-r", delayTime.to_string()};
			string[] spawn_env=Environ.get ();
			Pid child_pid;

			try{
				Process.spawn_async ("/", rebootArgs, spawn_env, SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD, null, out child_pid);
			}catch(SpawnError e){
				msgBox=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error has occurred while trying to reboot the computer..."));
				return;
			}

			cancelButton.set_sensitive(true);
			this.scheduledAction=true;
			this.notifyActionToUser(_("Rebooting"), _("The computer will reboot in ")+delayTime.to_string()+_(" minute(s)."));
		}

		private void cancelButtonAction(){
			string[] cancelArgs={"shutdown", "-c"};
			string[] spawn_env=Environ.get();
			Pid child_pid;

			try{
				Process.spawn_async ("/", cancelArgs, spawn_env, SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD, null, out child_pid);
			}catch(SpawnError e){
				msgBox=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error has occurred while trying to cancel the scheduled action..."));
				return;
			}

			cancelButton.set_sensitive(false);
			this.scheduledAction=false;
			this.notifyActionToUser(_("Action successfully cancelled."), _("The scheduled action has been canceled successfully."));
		}

		private void generateNotification(string notifID, string notifTitle, string notifMessage){
			Gtk.Application? app=get_application();
			Notification notification = new Notification ("PowerDown - Vinari Software");
			notification.set_priority(HIGH);
			notification.set_title(notifTitle);
			notification.set_body(notifMessage);
			app.send_notification(notifID, notification);
		}

		private bool checkInit(){
			string ls_stdout;
			string ls_stderr;
			int ls_status;

			try{
				GLib.Process.spawn_command_line_sync ("ps -p 1 -o comm=", out ls_stdout, out ls_stderr, out ls_status);
				ls_stdout=ls_stdout.strip();

				if(ls_stdout=="systemd"){
					return true;
				}
			}catch (SpawnError e) {
				print ("Unexpected error encountered: %s\n", e.message);
				return false;
			}

			return false;
		}
    }
}
